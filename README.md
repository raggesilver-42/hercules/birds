# Hercules - Stymphalian Birds

Easily start projects.

Available languages: c, node

```
usage: birds [OPTIONS...] <target directory>

    --author: creates an author file at the root of the project
    --help:   shows help
    --lib:    configures the project as a library (C only)
    --libft:  imports libft (C only, ignored by no-git)
    --no-git: does not initialize git project

```

## Install
Birds is installed in `~/.bin` and it's resources are installed in `~/.birds`

```bash
git clone --recursive git@gitlab.com:raggesilver-42/hercules/birds.git
# or
git clone --recursive https://gitlab.com/raggesilver-42/hercules/birds.git

cd birds
make install -j
```

## Uninstall
```bash
make uninstall
```

**Note** beware the uninstall rule will permanently delete `~/bin/birds` and
`~/.birds`.
