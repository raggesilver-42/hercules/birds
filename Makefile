NAME := birds
# CC ?= gcc

TEMPLATES_PATH ?=$(shell pwd)/templates

CFLAGS := -Wall -Werror -Wextra -Ofast

CFLAGS += -DTEMPLATES_PATH="\"${TEMPLATES_PATH}\""

OBJDIR := build
SRCDIR := src
HEADIR := includes

SRCS := $(shell find $(SRCDIR) -type f -name "*.c" $(DISABLED))
OBJS := $(SRCS:%=$(OBJDIR)/%.o)
DEPS := $(SRCS:%=$(OBJDIR)/%.d)

HEAD := $(shell find $(SRCDIR) -name "*.h" -and ! -name "*_priv.h")
HEAD := $(subst $(SRCDIR),$(HEADIR),$(HEAD))

LIBS := libs/libft/libft.a
LIBINCS := $(foreach lib,$(LIBS),-I$(dir $(lib))includes)

.PHONY: all re clean fclean debug $(LIBS) _$(NAME)

all: _$(NAME)

_$(NAME): $(LIBS)
	@$(MAKE) $(NAME)

$(NAME): $(HEAD) $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LIBINCS) $(LIBS)

$(OBJDIR) $(HEADIR):
	@mkdir -p $@

-include $(DEPS)

$(OBJDIR)/%.c.o: %.c Makefile
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -MMD -MP -c $< -o $@ $(LIBINCS)

$(HEADIR)/%.h:
	@mkdir -p $(dir $@)
	cp $(subst $(HEADIR),$(SRCDIR),$@) $@

$(LIBS):
	@$(MAKE) -C $(dir $@) $(MAKECMDGOALS)

clean:
	@$(foreach dep, $(LIBS), $(MAKE) -C $(dir $(dep)) clean)
	rm -f $(OBJS)
	rm -f $(DEPS)
	rm -rf $(OBJDIR)

fclean: clean
	@$(foreach dep, $(LIBS), $(MAKE) -C $(dir $(dep)) fclean)
	rm -f $(NAME)
	rm -rf $(HEADIR)

re: fclean
	@$(MAKE) all

debug: fclean
	@$(MAKE) all CFLAGS="$(CFLAGS) -g"

ci: MAKECMDGOALS=
ci: all
	valgrind --error-exitcode=1 --leak-check=full --show-leak-kinds=all --track-origins=yes ./$(NAME) --author --libft --lib test

install:
	@$(MAKE) fclean
	mkdir -p ~/.bin
	mkdir -p ~/.birds
	cp -r templates ~/.birds
	@TEMPLATES_PATH="~/.birds/templates" $(MAKE)
	cp $(NAME) ~/.bin/
	@echo "===================================================================="
	@echo "Make sure ~/.bin is in your path. You may use this"
	@echo 'export PATH="$$HOME/.bin:$$PATH"'
	@echo "===================================================================="

uninstall:
	@rm -rf ~/.birds
	@rm -rf ~/.bin/birds
