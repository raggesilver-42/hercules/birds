
# args = <path> <flags>
path=$1
name=$(basename "$path")
flags=$(($2))
_pwd=$(dirname $BASH_SOURCE[0])

FLAG_NO_GIT=$(($flags & 1))
FLAG_AUTHOR=$(($flags & 8))

mkdir -p $(dirname $path) || exit $?

unzip -j "$_pwd/template.zip" -d $path || exit $?

cd "$path"

if ! (($FLAG_NO_GIT)); then
	git init || exit $?
fi

if (($FLAG_AUTHOR)); then
	echo $USER > author || exit $?
fi

if [[ `command -v yarn` ]]; then
	yarn init -y || exit $?
else
	npm init -y || exit $?
fi
