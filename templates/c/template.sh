LIBFT_REPO="https://gitlab.com/raggesilver-42/libft"

# args = <path> <flags>
path=$1
name=$(basename "$path")
flags=$(($2))
_pwd=$(dirname $BASH_SOURCE[0])

FLAG_NO_GIT=$(($flags & 1))
FLAG_LIB=$(($flags & 2))
FLAG_LIBFT=$(($flags & 4))
FLAG_AUTHOR=$(($flags & 8))

mkdir -p $(dirname $path) || exit $?

if (($FLAG_LIB)); then
	unzip -j "$_pwd/lib_template.zip" -d $path || exit $?
else
	unzip -j "$_pwd/exe_template.zip" -d $path || exit $?
fi

cd "$path"

if ! (($FLAG_NO_GIT)); then
	git init || exit $?
	if (($FLAG_LIBFT)); then
		mkdir libs || exit $?
		git submodule add $LIBFT_REPO libs/libft || exit $?
	fi
fi

if (($FLAG_AUTHOR)); then
	echo $USER > author || exit $?
fi
