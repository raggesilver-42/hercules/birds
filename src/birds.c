/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   birds.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 23:05:50 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/25 00:06:17 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "birds.h"

const char	*g_bd_flags[] = (const char*[]){
	"no-git",
	"lib",
	"libft",
	"author",
	"help",
	NULL
};

const char	*g_bd_langs[] = (const char*[]){
	"c",
	"node",
	NULL
};

static int
	shift(t_bd_project *proj)
{
	++proj->argv;
	--proj->argc;
	return (TRUE);
}

static int
	parse_flags(t_bd_project *proj)
{
	ssize_t aux;

	while (*proj->argv)
	{
		if (ft_strequ(*proj->argv, "--") && shift(proj))
			break ;
		if (!ft_strnequ(*proj->argv, "--", 2))
			break ;
		(*proj->argv) += 2;
		if ((aux = ft_strv_str(g_bd_flags, *proj->argv)) > -1)
			proj->flags |= (1 << aux);
		else if (ft_strequ(*proj->argv, "lang"))
		{
			shift(proj);
			if (!*proj->argv ||
				(aux = ft_strv_str(g_bd_langs, *proj->argv)) == -1)
				die(1, "birds: Invalid language '%s'\n", *proj->argv);
			proj->lang = aux;
		}
		else
			die(1, "birds: Invalid flag '%s'\n", *proj->argv);
		shift(proj);
	}
	return (TRUE);
}

static void
	check_path(t_bd_project *proj)
{
	if (path_exists(proj->path))
		die(1, "birds: Path '%s' exists\n", proj->path);
	else if (errno != 0 && errno != ENOENT)
		die(1, "birds: '%s': %s\n", proj->path, strerror(errno));
}

static int
	birds(t_bd_project *proj)
{
	t_string	*cmd;
	int			res;

	cmd = ft_sprintf("/bin/bash " TEMPLATES_PATH "/%s/template.sh %s %lu",
		g_bd_langs[proj->lang], proj->path, proj->flags);
	res = system(cmd->data);
	ft_string_destroy(&cmd);
	return (res);
}

int
	main(int argc, const char **argv)
{
	t_bd_project	proj;

	proj = (t_bd_project){ NULL, NULL, 0, BD_LANG_C, --argc, ++argv };
	if (!parse_flags(&proj))
		return (1);
	if (proj.flags & BD_FLAG_HELP)
		return (print_help(0));
	if (proj.argc < 1)
		die(1, "birds: Missing target directory\n");
	else if (proj.argc > 1)
		die(1, "birds: Too many targets\n");
	proj.path = *proj.argv;
	proj.name = ft_basename(*proj.argv);
	check_path(&proj);
	return (birds(&proj));
}
