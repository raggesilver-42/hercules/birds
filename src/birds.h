/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   birds.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 21:39:59 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/25 00:06:24 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BIRDS_H
# define BIRDS_H

# include "libft.h"
# include <errno.h>

# ifndef TEMPLATES_PATH
#  define TEMPLATES_PATH "."
# endif

/*
** For strerror
*/

# include <string.h>

enum				e_bd_flags
{
	BD_FLAG_NONE = 0,
	BD_FLAG_NO_GIT = 1 << 0,
	BD_FLAG_LIB = 1 << 1,
	BD_FLAG_LIBFT = 1 << 2,
	BD_FLAG_AUTHOR = 1 << 3,
	BD_FLAG_HELP = 1 << 4,
};

enum				e_bd_langs
{
	BD_LANG_C = 0,
	BD_LANG_NODE = 1,
};

typedef struct		s_bd_project
{
	const char		*path;
	const char		*name;
	size_t			flags;
	int				lang;
	int				argc;
	const char		**argv;
}					t_bd_project;

extern const char	*g_bd_flags[];
extern const char	*g_bd_langs[];

ssize_t				ft_strv_str(const char *const *strv, const char *str);

void				die(int status, const char *fmt,
						...) __attribute__((format(printf,2,3)));
int					print_help(int status);
int					path_exists(const char *path);

#endif
