/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aux.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 23:24:06 by pqueiroz          #+#    #+#             */
/*   Updated: 2020/01/24 23:27:34 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "birds.h"

/*
** ft_strv_str - Search for a string in a NULL terminated string array.
**
** @param {const char *const *} strv a NULL terminated string array
** @param {const char *} str the string to search in the array
** @returns {ssize_t} the index of the string or -1 if not found
*/

ssize_t
	ft_strv_str(const char *const *strv, const char *str)
{
	size_t	i;

	i = 0;
	while (strv[i])
	{
		if (ft_strequ(strv[i], str))
			return (i);
		++i;
	}
	return (-1);
}

/*
** Display an error message and exit the program with status `status`.
**
** @param {int} status the status to exit with
** @param {const char *} fmt ft_printf formatted error str
** @param {...} ... arguments for ft_printf
** @noreturn
*/

void __attribute__((noreturn))
	die(int status, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	ft_vdprintf(2, fmt, ap);
	exit(status);
}

int
	print_help(int status)
{
	ft_dprintf((status == 0) ? 1 : 2,
		"\nusage: birds [OPTIONS...] <target directory>\n\n"
		"    --author: creates an author file at the root of the project\n"
		"    --help:   shows help\n"
		"    --lib:    configures the project as a library (C only)\n"
		"    --libft:  imports libft (C only, ignored by no-git)\n"
		"    --no-git: does not initialize git project\n"
		"\n");
	return (status);
}

int
	path_exists(const char *path)
{
	errno = 0;
	if (access(path, F_OK) == 0)
		return (TRUE);
	else
		return (FALSE);
}
